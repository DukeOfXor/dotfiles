" to use this config you need to install the vim-plug plugin manager.
" vim-plug can be installed using this command:
" curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

" define plugins for vim-plug to install
" restart vim and run :PlugInstall to install newly added plugins
call plug#begin('~/.vim/plugged')

" NERDTree
Plug 'https://github.com/scrooloose/nerdtree.git'

" CtrlP
Plug 'https://github.com/ctrlpvim/ctrlp.vim.git'

" fugitive.vim
Plug 'https://github.com/tpope/vim-fugitive.git'

" vim-airline
Plug 'https://github.com/vim-airline/vim-airline.git'
" vim-airline-themes
Plug 'https://github.com/vim-airline/vim-airline-themes.git'

" SimplyFold
Plug 'https://github.com/tmhedberg/SimpylFold.git'

" indentpython.vim
Plug 'https://github.com/vim-scripts/indentpython.vim.git'

" YouCompleteMe
" Plug 'https://github.com/Valloric/YouCompleteMe.git'

" syntastic
Plug 'https://github.com/vim-syntastic/syntastic.git'

" vim-flake8
Plug 'https://github.com/nvie/vim-flake8.git'

" gruvbox
Plug 'https://github.com/morhetz/gruvbox.git'

" end of plugin definition
call plug#end()

" start of actual config
syntax on
let python_highlight_all=1
set encoding=utf-8
set number
set nowrap
set nocompatible
set wildmenu
set cmdheight=2
set hidden
set hlsearch
set laststatus=2
set t_Co=256
set relativenumber
set background=dark
set ttimeoutlen=0
set foldmethod=indent
set foldlevel=99
set clipboard=unnamed

" set leader
let mapleader = " " 

" insert newlines without entering edit mode (Enter, Shift+Enter)
nmap <S-Enter> O<Esc>
nmap <CR> o<Esc>

" disabling arrow keys for habit breaking
noremap <Up> <NOP>
noremap <Down> <NOP>
noremap <Left> <NOP>
noremap <Right> <NOP>

" python with virtualenv support
py << EOF
import os
import sys
if 'VIRTUAL_ENV' in os.environ:
	project_base_dir = os.environ['VIRTUAL_ENV']
	activate_this = os.path.join(project_base_dir, 'bin/activate_this.py')
	execfile(activate_this, dict(__file__=activate_this))
EOF

" colorscheme
colorscheme gruvbox

" Plugin specific settings

" NERDTree config
map <C-x> :NERDTreeToggle<CR>
let NERDTreeMapActivateNode='l'
" close vim if last open window is NERDTree:wq
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
" open NERDTree if vim starts up with no files specified
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif

" vim-airline config
let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1
" vim-airline-themes config
let g:airline_theme='gruvbox'

" Folding config (SimplyFold)
nnoremap <space> za

" YouCompleteMe config
let g:ycm_autoclose_preview_window_after_completion=1
map <leader>g  :YcmCompleter GoToDefinitionElseDeclaration<CR>


" File specific settings

" *.py
" PEP8 indentation
au BufNewFile,BufRead *.py
	\ set tabstop=4 |
	\ set softtabstop=4 |
	\ set shiftwidth=4 |
	\ set textwidth=79 |
	\ set expandtab |
	\ set autoindent |
	\ set fileformat=unix

" *.py, *.pyw, *.c, *.h
" highlight extraneous whitespace
highlight BadWhitespace ctermbg=darkgreen guibg=darkgreen
au BufRead,BufNewFile *.py,*.pyw,*.c,*.h match BadWhitespace /\s\+$/
